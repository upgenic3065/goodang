<?php

namespace App\Http\Controllers;

class ApiStockOpnameStockItemListController extends \crocodicstudio\crudbooster\controllers\ApiController
{
    public function __construct()
    {
        $this->table = 'stock_opname_item';
        $this->permalink = 'stock_opname_stock_item_list';
        $this->method_type = 'post';
    }

    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query
    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process
        if ($result['api_status'] == 1) {
            foreach ($result['data'] as $item) {
                $item->item_name = tv($item->id_item, 'item', 'name');
                $item->item_sku = tv($item->id_item, 'item', 'sku');
            }
        }
    }
}
