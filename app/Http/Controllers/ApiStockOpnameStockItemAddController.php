<?php

namespace App\Http\Controllers;

use DB;

class ApiStockOpnameStockItemAddController extends \crocodicstudio\crudbooster\controllers\ApiController
{
    public function __construct()
    {
        $this->table = 'stock_opname_item';
        $this->permalink = 'stock_opname_stock_item_add';
        $this->method_type = 'post';
    }

    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
        $stock_opname_item = DB::table('stock_opname_item')->where('id_item', $postdata['id_item'])->where('id_stock_opname', $postdata['id_stock_opname'])->first();

        $qty = TotalQtyItem($postdata['id_item'], $postdata['qty'], $postdata['qty_type']);
        $postdata['qty'] = $qty;

        if ($stock_opname_item) {
            $p['qty'] = $qty + $stock_opname_item->qty;
            $update = DB::table('stock_opname_item')->where('id', $stock_opname_item->id)->update($p);

            if ($update) {
                $result['api_status'] = 1;
                $result['api_message'] = 'success';
                $res = response()->json($result);
                $res->send();
                exit;
            } else {
                $result['api_status'] = 0;
                $result['api_message'] = 'Failed';
                $res = response()->json($result);
                $res->send();
                exit;
            }
        }

        unset($postdata['qty_type']);
        unset($postdata['sku']);
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query
    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process
    }
}
