<?php

namespace App\Http\Controllers;

class ApiAssetBarangKeluarScanController extends \crocodicstudio\crudbooster\controllers\ApiController
{
    public function __construct()
    {
        $this->table = 'item_asset_code';
        $this->permalink = 'asset_barang_keluar_scan';
        $this->method_type = 'post';
    }

    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
        $assetDetailFromBarcodeOut = assetDetailFromBarcodeOut(g('code'), 1);
        $result = $assetDetailFromBarcodeOut;
        $res = response()->json($result);
        $res->send();
        exit;
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query
    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process
    }
}
