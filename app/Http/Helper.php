<?php

if (! function_exists('generate_sku')) {
    function generate_sku($type_item)
    {
        if ($type_item == 1) {
            $code = 'A';
        } else {
            $code = 'S';
        }
        $item = DB::table('item')->where('sku', 'LIKE', 'GD'.$code.date('Ymd').'%')->orderBy('id', 'DESC')->first();
        if ($item) {
            $urut = substr($item->sku, 11);
            $sku = 'GD'.$code.date('Ymd').sprintf('%03s', $urut + 1);
        } else {
            $sku = 'GD'.$code.date('Ymd').'001';
        }

        return $sku;
    }
}
if (! function_exists('generateLabelCode')) {
    function generateLabelCode($sku)
    {
        $item = DB::table('item_asset_code')->where('code', 'LIKE', '%'.$sku.'%')->orderBy('id', 'DESC')->first();
        if ($item) {
            $urut = substr($item->code, 14);
            $code = $sku.sprintf('%04s', $urut + 1);
        } else {
            $code = $sku.'0001';
        }

        return $code;
    }
}
if (! function_exists('tv')) {
    function tv($id, $table, $show = 'id')
    {
        $query = DB::table($table)->where('id', $id)->first();

        return $query->$show;
    }
}
if (! function_exists('tv_where')) {
    function tv_where($id, $table, $where = 'id', $show = 'id')
    {
        $query = DB::table($table)->where($where, $id)->first();

        return $query->$show;
    }
}
if (! function_exists('array_table')) {
    function array_table($table)
    {
        $query = DB::table($table)->orderBy('name', 'ASC')->get();

        return $query;
    }
}
if (! function_exists('addStock')) {
    function addStock($id_item, $qty, $id_warehouse)
    {
        $item = DB::table('item')->where('id', $id_item)->first();
        $check_on_stock = DB::table('stock')->where('id_item', $id_item)->where('id_warehouse', $id_warehouse)->first();
        if ($check_on_stock) {
            // update
            $final_qty = $check_on_stock->qty + $qty;
            $p['qty'] = $final_qty;
            $p['updated_at'] = date('Y-m-d H:i:s');
            $p['type'] = $item->type;
            DB::table('stock')->where('id_item', $id_item)->where('id_warehouse', $id_warehouse)->update($p);
        } else {
            // insert
            $p['id_item'] = $id_item;
            $p['id_warehouse'] = $id_warehouse;
            $p['qty'] = $qty;
            $p['updated_at'] = date('Y-m-d H:i:s');
            $p['type'] = $item->type;
            DB::table('stock')->insert($p);
        }
    }
}

if (! function_exists('assetDetailFromBarcodeOut')) {
    function assetDetailFromBarcodeOut($barcode, $api = 0)
    {
        $query = DB::table('item_asset_code')
                ->select('item_asset_code.*', 'item.sku as sku', 'item.name as name', 'item.description as description')
                ->join('item', 'item.id', '=', 'item_asset_code.id_item')
                ->where('item_asset_code.code', $barcode)
                ->where('item.type', 1)
                ->first();

        if ($query) {
            $check_item_in = DB::table('item_in_asset')->where('code', $barcode)->where('id_item_out_asset', null)->first();
            $gudang = tv($check_item_in->id_warehouse, 'warehouse', 'name');
            $query->gudang = $gudang;

            if ($check_item_in) {
                $result['api_status'] = 1;
                if ($api == 0) {
                    $result['api_message'] = "<div class='text-success'>Item can be issued</div>";
                } else {
                    $result['api_message'] = 'Item can be issued';
                }
                $result['data'] = $query;
            } else {
                $result['api_status'] = 0;
                if ($api == 0) {
                    $result['api_message'] = "<div class='text-danger'>Item not registered at warehouse</div>";
                } else {
                    $result['api_message'] = 'Item not registered at warehouse';
                }
            }
        } else {
            $result['api_status'] = 0;
            if ($api == 0) {
                $result['api_message'] = "<div class='text-danger'>Barcode not registered</div>";
            } else {
                $result['api_message'] = 'Barcode not registered';
            }
        }

        return $result;
    }
}

if (! function_exists('assetDetailFromBarcode')) {
    function assetDetailFromBarcode($barcode, $api = 0)
    {
        $query = DB::table('item_asset_code')
                ->select('item_asset_code.*', 'item.sku as sku', 'item.name as name', 'item.description as description')
                ->join('item', 'item.id', '=', 'item_asset_code.id_item')
                ->where('item_asset_code.code', $barcode)
                ->where('item.type', 1)
                ->first();
        if ($query) {
            $check_item_in = DB::table('item_in_asset')->where('code', $barcode)->where('id_item_out_asset', null)->first();
            $gudang = tv($check_item_in->id_warehouse, 'warehouse', 'name');
            if ($check_item_in) {
                $result['api_status'] = 0;
                if ($api == 0) {
                    $result['api_message'] = "<div class='text-danger'>Item has entered the warehouse ".$gudang.'</div>';
                } else {
                    $result['api_message'] = 'Item has entered the warehouse '.$gudang;
                }
            } else {
                $result['api_status'] = 1;
                if ($api == 0) {
                    $result['api_message'] = "<div class='text-success'>Barcodes can be entered</div>";
                } else {
                    $result['api_message'] = 'Barcodes can be entered';
                }

                $result['data'] = $query;
            }
        } else {
            $result['api_status'] = 0;
            if ($api == 0) {
                $result['api_message'] = "<div class='text-danger'>Barcode not registered</div>";
            } else {
                $result['api_message'] = 'Barcode not registered';
            }
        }

        return $result;
    }
}

if (! function_exists('stockItem')) {
    function stockItem($id_item, $id_warehouse = 0)
    {
        if ($id_warehouse == 0) {
            $query = DB::table('stock')->where('id_item', $id_item)->sum('qty');
        } else {
            $query = DB::table('stock')->where('id_item', $id_item)->where('id_warehouse', $id_warehouse)->sum('qty');
        }

        return $query;
    }
}

if (! function_exists('totalSoAsset')) {
    function totalSoAsset($id_so)
    {
        $query = DB::table('stock_opname_asset_item')->where('id_stock_opname_asset', $id_so)->count();

        return $query;
    }
}

if (! function_exists('totalSoStock')) {
    function totalSoStock($id_so)
    {
        $query = DB::table('stock_opname_item')->where('id_stock_opname', $id_so)->count();

        return $query;
    }
}

if (! function_exists('totalSoAssetPerItem')) {
    function totalSoAssetPerItem($id_so, $id_item)
    {
        $query = DB::table('stock_opname_asset_item')->where('id_stock_opname_asset', $id_so)->where('id_item', $id_item)->count();

        return $query;
    }
}

if (! function_exists('lastInsertSoAssetItem')) {
    function lastInsertSoAssetItem($id_so, $id_item, $show = 'created_at')
    {
        $query = DB::table('stock_opname_asset_item')->where('id_stock_opname_asset', $id_so)->where('id_item', $id_item)->orderby('id', 'desc')->first();

        return $query->$show;
    }
}

if (! function_exists('stockItem')) {
    function stockItem($id_item)
    {
        $query = DB::table('stock')->where('id_item', $id_item)->sum('qty');

        return $query;
    }
}
if (! function_exists('stockItemPerWarehouse')) {
    function stockItemPerWarehouse($id_item, $id_warehouse)
    {
        $query = DB::table('stock')->where('id_item', $id_item)->where('id_warehouse', $id_warehouse)->sum('qty');

        return $query;
    }
}
if (! function_exists('checkSkuItem')) {
    function checkSkuItem($sku)
    {
        $item = DB::table('item')->where('sku', $sku)->first();
        if (! $item) {
            $item_qty_type = DB::table('item_qty_type')->where('sku', $sku)->first();
            if ($item_qty_type) {
                $item = DB::table('item')->where('id', $item_qty_type->id_item)->first();
                $item->qty_type = $item_qty_type->qty_type;
                $item->qty_type_count = $item_qty_type->qty;
                $item->sku = $sku;
            } else {
                return null;
            }
        } else {
            $item->qty_type = 'PCS';
            $item->qty_type_count = 1;
            $item->sku = $sku;

            $a = [];
            $a[] = ['qty_type' => 'PCS'];
            $x = DB::table('item_qty_type')->select('qty_type')
                    ->where('id_item', $item->id)->get();

            foreach ($x as $x) {
                $a[] = ['qty_type' => $x->qty_type];
            }

            $item->select_qty_type = $a;

            // print_r($a);exit;
        }

        return $item;
    }
}
if (! function_exists('TotalQtyItem')) {
    function TotalQtyItem($id_item, $qty, $qty_type)
    {
        if ($qty_type == 'PCS') {
            $total = $qty;
        } else {
            $item_qty_type = DB::table('item_qty_type')->where('id_item', $id_item)->where('qty_type', $qty_type)->first();
            $total = $qty * $item_qty_type->qty;
        }

        return $total;
    }
}
if (! function_exists('stockOpnameAssetPublishCondition')) {
    function stockOpnameAssetPublishCondition($id)
    {
        $soa = DB::table('stock_opname_asset')->where('id', $id)->first();
        $soai = DB::table('stock_opname_asset_item')->where('id_stock_opname_asset', $id)->get();

        // jadikan semua stock di gudang menjadi 0
        $s['qty'] = 0;
        DB::table('stock')->where('id_warehouse', $soa->id_warehouse)->where('type', 1)->update($s);

        foreach ($soai as $soai_update_nol) {
            // keluarkan semua barang masuk yang ada di gudang
            $p['id_item_out_asset'] = 0;
            $p['id_stock_opname_asset'] = $id;
            DB::table('item_in_asset')->where('id_item', $soai_update_nol->id_item)->where('id_warehouse', $soa->id_warehouse)->update($p);
        }

        foreach ($soai as $soai) {
            // masukan semua SO ke barang masuk
            $q['code'] = $soai->code;
            $q['id_item'] = $soai->id_item;
            $q['id_warehouse'] = $soa->id_warehouse;
            $q['description'] = 'From Stock Opname';
            $q['id_cms_users'] = $soai->id_cms_users;
            $q['id_item_out_asset'] = null;
            $q['created_at'] = date('Y-m-d H:i:s');
            $q['id_stock_opname_asset'] = $id;
            DB::table('item_in_asset')->insert($q);

            // update Stock
            addStock($soai->id_item, 1, $soa->id_warehouse);
        }
    }
}

if (! function_exists('stockOpnameStockPublishCondition')) {
    function stockOpnameStockPublishCondition($id)
    {
        $stock_opname = DB::table('stock_opname')->where('id', $id)->first();
        $stock_opname_item = DB::table('stock_opname_item')->where('id_stock_opname', $id)->get();

        // Buat Semua stock di warehouse 0
        $p_s['qty'] = 0;
        $p_s['updated_at'] = date('Y-m-d H:i:s');
        DB::table('stock')->where('id_warehouse', $stock_opname->id_warehouse)->where('type', 2)->update($p_s);

        // update semua stock dgn yg baru
        foreach ($stock_opname_item as $soi) {
            addStock($soi->id_item, $soi->qty, $stock_opname->id_warehouse);
        }
    }
}

if (! function_exists('minimalStock')) {
    function minimalStock($id_item, $id_warehouse = 0)
    {
        $query = DB::table('item_minimal_stock')->where('id_item', $id_item);
        if ($id_warehouse != 0) {
            $query = $query->where('id_warehouse', $id_warehouse);
        }
        $query = $query->sum('minimal_stock');

        return $query;
    }
}

if (! function_exists('warehouseUser')) {
    function warehouseUser($id_cms_users = 0)
    {
        $warehouse_users = DB::table('warehouse_users')->where('id_cms_users', $id_cms_users)->first();
        if (! $warehouse_users) {
            $query = DB::table('warehouse')->get();
        } else {
            $id_warehouse = DB::table('warehouse_users')->where('id_cms_users', $id_cms_users)->pluck('id_warehouse');
            $query = DB::table('warehouse')->whereIn('id', $id_warehouse)->get();
        }

        return $query;
    }
}
if (! function_exists('idWarehouseUser')) {
    function idWarehouseUser($id_cms_users = 0)
    {
        $warehouse_users = DB::table('warehouse_users')->where('id_cms_users', $id_cms_users)->first();
        if (! $warehouse_users) {
            $query = DB::table('warehouse')->pluck('id');
        } else {
            $id_warehouse = DB::table('warehouse_users')->where('id_cms_users', $id_cms_users)->pluck('id_warehouse');
            $query = DB::table('warehouse')->whereIn('id', $id_warehouse)->pluck('id');
        }

        return $query;
    }
}

if (! function_exists('setUpdatedAt')) {
    function setUpdatedAt($where, $valueWhere, $table)
    {
        $p['updated_at'] = date('Y-m-d H:i:s');
        $query = DB::table($table)->where($where, $valueWhere)->update($p);
    }
}

if (! function_exists('lastUpdateItem')) {
    function lastUpdateItem($id_item, $id_warehouse = 0)
    {
        $check = DB::table('stock')->where('id_item', $id_item)->first();
        if ($check) {
            if ($id_warehouse == 0) {
                $stock = DB::table('stock')->where('id_item', $id_item)->orderby('updated_at', 'DESC')->first();
            } else {
                $stock = DB::table('stock')->where('id_item', $id_item)->where('id_warehouse', $id_warehouse)->orderby('updated_at', 'DESC')->first();
            }
            $updated_at = $stock->updated_at;
        } else {
            $updated_at = tv($id_item, 'item', 'created_at');
        }

        return $updated_at;
    }
}
