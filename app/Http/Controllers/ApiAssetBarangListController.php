<?php

namespace App\Http\Controllers;

class ApiAssetBarangListController extends \crocodicstudio\crudbooster\controllers\ApiController
{
    public function __construct()
    {
        $this->table = 'item';
        $this->permalink = 'asset_barang_list';
        $this->method_type = 'post';
    }

    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query
        if (g('search')) {
            $query = $query->where('name', 'LIKE', '%'.g('search').'%');
        }
    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process

        // $result->id_cms_users = tv($result->id,'item','id_cms_users');

        foreach ($result['data'] as $item) {
            $item->stock = stockItem($item->id);
            $item->id_cms_users = tv($item->id, 'item', 'id_cms_users');
            $item->created_at = lastUpdateItem($item->id);
        }
    }
}
